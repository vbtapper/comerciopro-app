const hapi                  = require('hapi');
const Hoek                  = require('hoek');
const routes                = require('./routes');
const Path                  = require('path');
const Inert                 = require('inert');

// Create hapi server instance
var server = new hapi.Server();

// add connection parameters
server.connection({  
    port: 3018,
    routes: {
        files: {
            relativeTo: Path.join(__dirname, 'views')
        }
    }
});

//registering
server.register(Inert, () => {});

var happiError = require('hapi-error');
const configErrors = {
  statusCodes: {
    500: { 
        message: 'Occoreu um erro interno',
        errorTitle: 'Erro'
    },
    404: { 
        message: 'Página inexistente',
        errorTitle: 'Erro'
    }
  }
};
happiError.options = configErrors;

server.register([happiError, require('vision')], (err) => {

    Hoek.assert(!err, err);

    server.views({
        engines: {
            html: require('handlebars'), 
        },
        path: 'views',
        relativeTo: __dirname,
        layoutPath: 'views/layout',
        helpersPath: 'views/helpers',
        partialsPath: 'views/partials'
    });
});

server.register({
  register: require('hapi-server-session'),
  options: {
    cookie: {
      isSecure: false,
    },
  },
}, function (err) { 
    if (err) { 
        Hoek.assert(!err, 'no errors registering plugins');
    } 
});

server.route(routes);

server.start(function (err) {
    // Log to the console the host and port info
    Hoek.assert(!err, 'no errors starting server');
    console.log('Server started at: ' + server.info.uri);
});