const LoginController                       = require('./Controllers/LoginController.js');
const DashboardController                   = require('./Controllers/DashboardController.js');
const TeamController                        = require('./Controllers/TeamController');
const ModelsController                      = require('./Controllers/ModelsController');
const ProductController                     = require('./Controllers/ProductController');

var Joi                                     = require('joi');

module.exports = [{
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },

    {
        method: 'GET',
        path: '/',
        handler: LoginController.RenderLoginPage,
    },

    {
        method: 'GET',
        path: '/models',
        handler: ModelsController.RenderModelsPage
    },

    {
        method: 'GET',
        path: '/ftlogin/{param*}',
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },
    {
        //Regex password
        method: 'POST',
        path: '/authUser',
        config : {
            handler: LoginController.Login,
            validate: {
                payload: {
                    email : Joi.string().email().required(),
                    password : Joi.string().min(5).max(25).required(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },

    {
        method: 'GET',
        path: '/logout',
        handler: LoginController.LogOut
    },

    {
        method: 'GET',
        path: '/ftlogin/{email}',
        handler: LoginController.RenderFirstTimeLogin
    },

    {
        method: 'POST',
        path: '/ftlogin/changepswd',
        config : {
            handler: LoginController.ChangePassword,
            validate: {
                payload: {
                    email : Joi.string().email().required(),
                    newpassword : Joi.string().min(5).max(25).required(),
                    passwordconf: Joi.string().min(5).max(25).required()
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },

    {
        method: 'GET',
        path: '/dashboard',
        handler: DashboardController.RenderDashboardPage,
    },

    {
        method: 'GET',
        path: '/team',
        handler: TeamController.RenderTeamPage
    },

    {
        method: 'GET',
        path: '/getteams',
        config: {
            handler: TeamController.GetMembers,
        }
    },

    {
        method: 'PATCH',
        path: '/enablemember',
        config: {
            validate: {
                payload: {
                    memberid: Joi.string().required()
                }
            },
            handler: TeamController.EnableMember
        }
    },

    {
        method: 'PATCH',
        path: '/disablemember',
        config: {
            validate: {
                payload: {
                    memberid: Joi.string().required()
                }
            },
            handler: TeamController.DisableMember
        }
    },

    {
        method: 'PATCH',
        path: '/removemember',
        config: {
            validate: {
                payload: {
                    memberid: Joi.string().required()
                }
            },
            handler: TeamController.RemoveMember
        }
    },

    {
        method: 'GET',
        path: '/newmember',
        handler: TeamController.RenderAddNewMember
    }, 

    {
        method: 'POST',
        path: '/addnewmember',
        config: {
            validate: {
                payload: {
                    firstname: Joi.string().required(),
                    lastname: Joi.string().required(),
                    email: Joi.string().email().required(),
                    role: Joi.string().valid('FINANCES', 'OPERATOR')
                }
            },
            handler: TeamController.AddNewMember
        }
    },

    {
        method: 'GET',
        path: '/newproduct',
        handler: ProductController.RenderNewProductPage
    },

    {
        method: 'POST',
        path: '/uploadimg',
        config: {
            payload: {
                output:'stream',
                parse: true,
                allow: 'multipart/form-data',
            },
            validate: {
                payload: {
                    file: Joi.object(),
                }
            },
            handler: ProductController.UploadImagesProcessor
        }
    },

    {
        method: 'POST',
        path: '/removeimg',
        config : {
            validate: {
                payload: {
                    file: Joi.string().required()
                }
            },
            handler: ProductController.RemoveImagesProcessor
        }
    },

    {
        method: 'POST',
        path: '/addproduct',
        config: {
            validate: {
                payload: {
                    productName: Joi.string().required(),
                    titleTag: Joi.string().required(),
                    descriptionTag: Joi.string().required(),
                    keywordsTag: Joi.string(),
                    description: Joi.string().required(),
                    model: Joi.string().required(),
                    category: Joi.string().required(),
                    brand: Joi.string().required(),
                    location: Joi.string().required(),
                    quantity: Joi.number().required(),
                    price: Joi.string().required(),
                    manufacture: Joi.string().required(),
                }
            },
            handler: ProductController.AddProduct
        }
    },

    {
        method: 'GET',
        path: '/products',
        handler: ProductController.RenderListProductsPage
    },

    {
        method: 'GET',
        path: '/getproducts',
        handler: ProductController.GetProducts,
    },

    {
        method: 'POST',
        path: '/addproductattribute',
        config: {
            validate: {
                payload: {
                    type: Joi.string().required(),
                    value: Joi.string().required(),
                    key: Joi.string(),
                    quantity: Joi.number().required(),
                    additionalCost: Joi.string(),
                    purchaseOption: Joi.boolean().required()
                }
            },
            handler: ProductController.AddProductAttribute
        }
    },

    {
        method: 'PATCH',
        path: '/removeproductattribute',
        config: {
            validate: {
                payload: {
                    id: Joi.string().required()
                }
            },
            handler: ProductController.RemoveProductAttribute
        }
    },

    {
        method: 'GET',
        path: '/products/{param*}',
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },

    {
        method: 'GET',
        path: '/products/{productID}',
        handler: ProductController.RenderViewProductPage
    },

    {
        method: 'PATCH',
        path: '/products/publishproduct',
        config: {
            validate: {
                payload: {
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.PublishProduct
        }
    },

    {
        method: 'PATCH',
        path: '/products/unpublishproduct',
        config: {
            validate: {
                payload: {
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.UnPublishProduct
        }
    },

    {
        method: 'PATCH',
        path: '/products/remove',
        config: {
            validate: {
                payload: {
                    productID: Joi.string().required()
                }
            },
            handler: ProductController.RemoveProduct
        }
    },

    {
        method: 'GET',
        path: '/products/edit/{param*}',
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },

    {
        method: 'GET',
        path: '/products/edit/{productID}',
        handler: ProductController.RenderEditProductPage
    },

    {
        method: 'PATCH',
        path: '/products/edit/removeimage',
        config: {
            validate: {
                payload: {
                    imgID: Joi.string().required()
                }
            },
            handler: ProductController.EditProductRemoveImage
        }
    },
    {
        method: 'POST',
        path: '/products/edit/uploadimg',
        config: {
            payload: {
                output:'stream',
                parse: true,
                allow: 'multipart/form-data',
            },
            validate: {
                payload: {
                    file: Joi.object(),
                }
            },
            handler: ProductController.EditProductUploadImagesProcessor
        }
    },

    {
        method: 'PATCH',
        path: '/products/edit/changeimgposition',
        config: {
            validate: {
                payload: {
                    position: Joi.number().required(),
                    positionToGo: Joi.number().required(),
                    imageID: Joi.string().required(),
                }
            },
            handler: ProductController.EditProductChangeImagePosition
        }
    },

    {
        method: 'PATCH',
        path: '/products/edit/removeattribute',
        config: {
            validate: {
                payload: {
                    attributeID: Joi.string().required()
                }
            },
            handler: ProductController.EditProductRemoveAttribute
        }
    },

    {
        method: 'POST',
        path: '/products/edit/addeditproductattribute',
        config: {
            validate: {
                payload: {
                    type: Joi.string().required(),
                    value: Joi.string().required(),
                    key: Joi.string(),
                    quantity: Joi.number().required(),
                    additionalCost: Joi.string(),
                    purchaseOption: Joi.boolean().required()
                }
            },
            handler: ProductController.EditProductAddNewAttribute
        }
    },

    {
        method: 'PATCH',
        path: '/products/edit/editdata',
        config: {
            validate: {
                payload: {
                    location: Joi.string().required(),
                    price: Joi.string().required(),
                    quantity: Joi.number().required(),
                    manufacture: Joi.string().required(),
                }
            },
            handler: ProductController.EditProductEditData
        }
    },

    {
        method: 'PATCH',
        path: '/products/edit/editgeneral',
        config: {
            validate: {
                payload: {
                    productName: Joi.string().required(),
                    titleTag: Joi.string().required(),
                    descriptionTag: Joi.string().required(),
                    description: Joi.string().required(),
                    keywordsTag: Joi.string().required()
                }
            },
            handler: ProductController.EditProductEditGeneral
        }
    },


]

