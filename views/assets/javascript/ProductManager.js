"use strict";

var numberOfFiles = 0;

$(document).ready(function(){

    $("#newProductSpinner").css('visibility', 'hidden');

    var myDropzone = $("#EditProductAddNewImage").dropzone({
        url:"uploadimg",
        paramName:"file",
        maxFiles: 10,
        acceptedFiles: "image/*",
        addRemoveLinks: false,
        maxFilesize:2,
        maxThumbnailFilesize:.5,
        dictDefaultMessage:"<i class='icon-dz fa fa-files-o'></i>Arraste as imagens para essa area ou clique para selecionar",
        success: function(file, response){
            $("#EditProductImagesTable > tbody").empty();
            response['array'].forEach(function(element) {
                $("#EditProductImagesTable > tbody").append("<tr><td class='text-center'><img src='" + element.path + "' width='100' alt='' class='img-thumbnail img-responsive'></td><td>" + "<input type='text' value='" + element.path +"' disabled name='txtImageUrl' class='form-control'></td><td><ul style='list-style-type: none;'><li style='display: block; padding: 1%'><input type='text' value='" +element.position + "' id='txtEditProductIamgesSortOrder"+element.position+"'class='form-control'></li><li style='display: block; padding: 1%'><button class='btn btn-sm btn-outline btn-info' type='button' onclick='EditProductChangeImagePosition(\"" +element.position+ "\",\"" + element.name + "\");'><i class='ti-save'></i></button></li></ul></td><td class='text-center'><button class='btn btn-sm btn-outline btn-danger' type='button' onclick='EditProductRemoveImage(\"" +element.name+ "\");'><i class='ti-trash'></i></button></td></tr>");
            });
        },
    })

    myDropzone.on("complete", function(file) {
        myDropzone.removeFile(file);
    });

    $('#EditProductAddNewImage').on('filebatchuploaderror', function(event, data, previewId, index) {
        alert('error')
    });


    $('#EditProductAddNewImage').on('filebatchuploadsuccess', function(event, data, previewId, index) {
        alert('success')
    });

    $('body').on('change', '#ddlNewProductAttributeType', function(){
        if(this.value === "color") {
            $("#newProductAttributeValueArea").empty();
            $("#newProductAttributeValueArea").append('<div id="newProductAttributeValueColor" class="input-group colorpicker-component">');
            $("#newProductAttributeValueColor").append('<input type="text" id="newProductAttributeColorRealValue" value="#00AABB" class="form-control" />');
            $("#newProductAttributeValueColor").append('<span class="input-group-addon"><i></i></span>');

            var colorpicker = $('#newProductAttributeValueColor');
            colorpicker.colorpicker({
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            });
        }
        else if(this.value === "size") {
            $("#newProductAttributeValueArea").empty();
            $("#newProductAttributeValueArea").append('<div id="newProductAttributeValueSize" class="form-group">');
            $("#newProductAttributeValueSize").append('<label for="newProductAttributeRealValueSize">Valor</label>')
            $("#newProductAttributeValueSize").append("<input type='text' id='newProductAttributeRealValueSize' class='form-control'/>")    
        }
        else if(this.value === "other") {
            $("#newProductAttributeValueArea").empty();
            $("#newProductAttributeValueArea").append('<div id="newProductAttributeValueOther" class="form-group">');
            $("#newProductAttributeValueOther").append('<label for="newProductAttributeOtherKey">Nome do Tipo</label>');
            $("#newProductAttributeValueOther").append("<input type='text' id='newProductAttributeOtherKey' class='form-control'/>") ;
            $("#newProductAttributeValueOther").append('<label for="newProductAttributeOtherValue">Valor do Tipo</label>');
            $("#newProductAttributeValueOther").append("<input type='text' id='newProductAttributeOtherValue' class='form-control'/>");
        }
        else if(this.value === "none") {
            $("#newProductAttributeValueArea").empty();
        }
    });

    /**
     *  Attributes
     */

    $('body').on('click', '#btnAddNewProductAttribute', function(){
        var typeParam = $("#ddlNewProductAttributeType").val();
        var forSellParam = $("#chkNewProductAttributeForSell").prop("checked") ? true : false;
        var valueParam;

        if($("#ddlNewProductAttributeType").val() === "none") {
            swal("Atenção", "Por favor digite o tipo de atributo que deseja adicionar ao produto.", "warning");
            return;
        }
        else if($("#ddlNewProductAttributeType").val() === "size") {
            if(!checkEmpty($("#newProductAttributeRealValueSize").val())) {
                swal("Atenção", "Por favor digite o tamanho do atributo do producto", "warning");
                return;
            }
            else {
                valueParam = $("#newProductAttributeRealValueSize").val();
            }
        }
        else if($("#ddlNewProductAttributeType").val() === "color") {
            valueParam = $("#newProductAttributeColorRealValue").val();
        }
        else if($("#ddlNewProductAttributeType").val() === "other") {
            if(!checkEmpty($("#newProductAttributeOtherKey").val())) {
                swal("Atenção", "Por favor digite o nome do tipo de atributo do producto", "warning");
                return;
            }
            if(!checkEmpty($("#newProductAttributeOtherValue").val())) {
                swal("Atenção", "Por favor digite o valor do tipo de atributo do producto", "warning");
                return;
            }   
            typeParam = $("#newProductAttributeOtherKey").val();
            valueParam = $("#newProductAttributeOtherValue").val();
        }

        if(!checkEmpty($("#txtNewProductAttributeQuantity").val())) {
            swal("Atenção", "Por favor digite a quantidade de itens com este atributo", "warning");
            return;
        }

        $.ajax({
            type: "POST",
            url: "addproductattribute",
            async: false,
            data: { type: typeParam, value: valueParam, key: $("#txtNewProductAttributeKey").val(), additionalCost: $("#txtNewProductAttributeAddPrice").val(), purchaseOption: forSellParam, quantity: $("#txtNewProductAttributeQuantity").val() },
            success: function(response) { 
                if(response['statusCode'] != 200) {
                    swal("Erro", response['message'], "error");
                }
                else {
                    swal("Sucesso", response['message'], "success");

                    $("#newProductAttributeDisplayTable > tbody").empty();
                    response['array'].forEach(function(element) {
                        $("#newProductAttributeDisplayTable > tbody").append("<tr><td>" + element.type + "</td><td>" + element.value + "</td><td>"+element.key+"</td><td>" + element.quantity + "</td><td>" + element.additionalCost + "</td><td>" + element.purchaseOption + "</td><td><a class='btn btn-outline btn-primary' onclick='RemoveAttribute(\"" +element.id+ "\");'><i class='ti-trash'></i></a></td></tr>");
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(errorThrown === 'Bad Request') {
                    swal("Erro", 'Por favor insira seus dados correctamente', "error");
                }
                else if(errorThrown === 'Internal Server Error') {
                    swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                }
            }
        });

    })
     /**
      * end attributes
      */

    /**
     * New Product
     */
    $("#frmNewProduct").validate({highlight:function(r){$(r).closest(".form-group").addClass("has-error")},unhighlight:function(r){$(r).closest(".form-group").removeClass("has-error")},errorElement:"span",errorClass:"help-block",errorPlacement:function(r,e){e.parent(".input-group").length?r.insertAfter(e.parent()):e.parent("label").length?r.insertBefore(e.parent()):r.insertAfter(e)}})

    $("#frmNewProduct").steps({
        headerTag:"h3",
        bodyTag:"fieldset",
        transitionEffect:"slide",
        stepsOrientation:"vertical",
        labels: {
            cancel: "Cancelar",
            current: "current step:",
            pagination: "Pagination",
            finish: "Enviar",
            next: "Seguinte",
            previous: "Anterior",
            loading: "Carregando ..."
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if(currentIndex === 0) { 
                if(!checkEmpty($("#txtNewProductName").val())) {
                    swal("Erro", "Por favor digite um nome valido para o producto", "error");
                    return false;
                }

                if(!checkEmpty($("#txtNewProductMetaTagTitle").val())) {
                    swal("Erro", "Por favor digite um titulo para o producto.", "error");
                    return false;
                }

                if(!checkEmpty($("#txtNewProductMetaTagDescription").val())) {
                    swal("Erro", "Por favor digite uma descrição tag para ajudar o cliente a entender do que se trata.", "error");
                    return false;
                }

                if(!checkEmpty($("#txtNewProductDescription").val())) {
                    swal("Erro", "Por favor digite uma descrição para ajudar o cliente a entender o producto", "error");
                    return false;
                }

            }
            else if(currentIndex === 1) {
                if(!checkEmpty($("#txtNewProductModel").val())) {
                    swal("Erro", "Por favor digite o modelo do producto", "warning");
                    return false;
                }

                if(!checkEmpty($("#txtNewProductCategory").val())) {
                    swal("Erro", "Por favor digite a categoria do producto", "warning");
                    return false;
                }

                if(!checkEmpty($("#txtNewProductLocation").val())) {
                    swal("Erro", "Por favor digite a localização do producto", "warning");
                    return false;
                }

                if(!checkNumbersOnly($("#txtNewProductQuantity").val())) {
                    swal("Erro", "Por favor digite a quantidade válida em estoque do producto", "warning");
                    return false;
                }

                if(!checkMoney($("#txtNewProductSellingPrice").val())) {
                    swal("Erro", "Por favor digite o preço do producto correctamente", "warning");
                    return false;
                }
                
                if(!checkEmpty($("#txtNewProductManufacture").val())) {
                    swal("Erro", "Por favor digite o nome do fabricante do producto", "warning");
                    return false;
                }

                if(!checkEmpty($("#txtNewProductBrand").val())) {
                    swal("Erro", "Por favor digite a marca do producto corretamente", "warning");
                    return false;
                }
                
            }
            
            return true; 
        },
        onStepChanged: function (event, currentIndex, priorIndex) { },

        onCanceled: function (event) { },

        onFinishing: function (event, currentIndex) { 
            if(numberOfFiles === 0) {
                swal("Erro", "Por favor selecione no minimo uma imagem para identificação do producto", "error");
                return false;
            }
            else {
                $("#newProductSpinner").css('visibility', 'visible');
                var intv = setInterval(AddNewProduct, 1000);
                function AddNewProduct() {
                    $.ajax({
                        type: "POST",
                        url: "addproduct",
                        async: false,
                        data: { productName: $("#txtNewProductName").val(), titleTag: $("#txtNewProductMetaTagTitle").val(), descriptionTag: $("#txtNewProductMetaTagDescription").val(), keywordsTag: $("#txtNewProductMetaTagKeywords").val(), description: $("#txtNewProductDescription").val(), model: $("#txtNewProductModel").val(), category: $("#txtNewProductCategory").val(), brand: $("#txtNewProductBrand").val(), location: $("#txtNewProductLocation").val(), quantity: $("#txtNewProductQuantity").val(), price: $("#txtNewProductSellingPrice").val(), manufacture: $("#txtNewProductManufacture").val() },
                        success: function(response) { 
                            if(response['statusCode'] != 200) {
                                clearInterval(intv);
                                $("#newProductSpinner").css('visibility', 'hidden');
                                swal("Erro", response['message'], "error");
                            }
                            else {
                                clearInterval(intv);
                                $("#newProductSpinner").css('visibility', 'hidden');
                                window.location.href = response['message'];
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            clearInterval(intv);
                            $("#newProductSpinner").css('visibility', 'hidden');
                            if(errorThrown === 'Bad Request') {
                                swal("Erro", 'Por favor insira seus dados correctamente', "error");
                            }
                            else if(errorThrown === 'Internal Server Error') {
                                swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                            }
                        }
                    });
                }
            } 
        },
        
        onFinished: function (event, currentIndex) { },
    })

    /**
     * End New Product
     */

    /**
     * Product List
     */
    var proTable = $('#listproducts').dataTable({
        ajax :{
            url : "/getproducts",
            type: "GET"
        },
        "serverSide": false,
        "processing": true,
        "lengthChange":!1,
        "pageLength":10,
        "colReorder":!0,
        "aoColumns": [
            {
                "mData": null,
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return "<img src='"+ data['image'] +"' width='50' alt='' class='img-thumbnail img-responsive'>";
            
                }
            },
            {
                "mData" : "name"
            },
            {   
                "mData" : "model" 
            },
            {
                "mData" : "price"
            },
            {
                "mData" : "quantity"
            },
            {
                "mData" : "status"
            },
            {
                "mData": null,
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return "<a class='btn btn-outline btn-primary' href='/products/" +data['id']+ "'> <i class='ti-eye'></i></a>";
        
                }
            },
            {
                "mData": null,
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return "<a class='btn btn-outline btn-primary' href='products/edit/" +data['id']+ "'> <i class='ti-pencil'></i></a>";
                }
            },
            {
                "mData": null,
                "bSortable": false,
                "mRender": function(data, type, full) {
                    return "<a class='btn btn-outline btn-primary' onclick='RemoveProduct(\"" +data['id']+ "\");'> <i class='ti-trash'></i></a>";
                }
            }
        ],
        "language": {
                "lengthMenu": "Mostrando _MENU_ productos por página",
                "zeroRecords": "Não há productos registrados",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "Não há productos registrados",
                "infoFiltered": "(filtrados de _MAX_ productos)",
                "search": "",
                "paginate": {
                    "previous": "Anterior",
                    "next" : "Seguinte"
                },
                "loading": "carregando..."
        },
        "scroller": {
            loadingIndicator: true
        },
        "searching": false
    });
    /**
     * End Product List
     */

     /**
      * Publish Product
      */
    $('body').on('click', '#btnViewProductPublish', function(){
        var ProduIDParam = this.name;
        
        swal({   
        title: "Tem a certeza?",   
        text: "Publicando este producto, o mesmo aparecerá para vendas na sua loja virtual",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Sim, publica!",   
        cancelButtonText: "Não, cancela!",   
        closeOnConfirm: false,   
        closeOnCancel: false }, 
        function(isConfirm){   
            if (isConfirm) {     
                $.ajax({
                    type: "PATCH",
                    url: "publishproduct",
                    async: false,
                    data: { productID: ProduIDParam },
                    success: function(response) { 
                        if(response['statusCode'] != 200) {
                            swal("Erro", response['message'], "error");
                        }
                        else {
                            window.location.href = response['message'];
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                    }
                });
            }
            else {
                swal("Cancelado", "Operação cancelada com sucesso", "success");
            }
        });
    })

    $('body').on('click', '#btnViewProductUnPublish', function(){
        var ProduIDParam = this.name;
        
        swal({   
        title: "Tem a certeza?",   
        text: "Ocultando este producto, o mesmo não estará disponivel para vendas na sua loja virtual",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Sim, oculta!",   
        cancelButtonText: "Não, cancela!",   
        closeOnConfirm: false,   
        closeOnCancel: false }, 
        function(isConfirm){   
            if (isConfirm) {     
                $.ajax({
                    type: "PATCH",
                    url: "unpublishproduct",
                    async: false,
                    data: { productID: ProduIDParam },
                    success: function(response) { 
                        if(response['statusCode'] != 200) {
                            swal("Erro", response['message'], "error");
                        }
                        else {
                            window.location.href = response['message'];
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                    }
                });
            }
            else {
                swal("Cancelado", "Operação cancelada com sucesso", "success");
            }
        });
    })
    /**
     * Edn Publish PRoduct
     */

     /**
      * Edit Product
      */
    $('body').on('change', '#ddlEditProductAttributeType', function(){
        if(this.value === "color") {
            $("#editProductAttributeValueArea").empty();
            $("#editProductAttributeValueArea").append('<div id="EditProductAttributeValueColor" class="input-group colorpicker-component">');
            $("#EditProductAttributeValueColor").append('<input type="text" id="editProductAttributeColorRealValue" value="#00AABB" class="form-control" />');
            $("#EditProductAttributeValueColor").append('<span class="input-group-addon"><i></i></span>');

            var colorpicker = $('#EditProductAttributeValueColor');
            colorpicker.colorpicker({
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            });
        }
        else if(this.value === "size") {
            $("#editProductAttributeValueArea").empty();
            $("#editProductAttributeValueArea").append('<div id="editProductAttributeValueSize">');
            $("#editProductAttributeValueSize").append('<label for="editProductAttributeRealValueSize">Valor</label>')
            $("#editProductAttributeValueSize").append("<input type='text' id='editProductAttributeRealValueSize' class='form-control' />")    
        }
        else if(this.value === "other") {
            $("#editProductAttributeValueArea").empty();
            $("#editProductAttributeValueArea").append('<div id="editProductAttributeValueOther">');
            $("#editProductAttributeValueOther").append('<label for="editProductAttributeOtherKey">Nome do Tipo</label>');
            $("#editProductAttributeValueOther").append("<input type='text' id='editProductAttributeOtherKey' class='form-control'/>") ;
            $("#editProductAttributeValueOther").append('<label for="editProductAttributeOtherValue">Valor do Tipo</label>');
            $("#editProductAttributeValueOther").append("<input type='text' id='editProductAttributeOtherValue' class='form-control'/>");
        }
        else if(this.value === "none") {
            $("#editProductAttributeValueArea").empty();
        }
    });

    $('body').on('click', '#btnAddEditProductAttribute', function(){
        var typeParam = $("#ddlEditProductAttributeType").val();
        var forSellParam = $("#chkEditProductAttributeForSell").prop("checked") ? true : false;
        var valueParam;

        if($("#ddlEditProductAttributeType").val() === "none") {
            swal("Atenção", "Por favor digite/escolha o tipo de atributo que deseja adicionar ao produto.", "warning");
            return;
        }
        else if($("#ddlEditProductAttributeType").val() === "size") {
            if(!checkEmpty($("#editProductAttributeRealValueSize").val())) {
                swal("Atenção", "Por favor digite o tamanho do atributo do producto", "warning");
                return;
            }
            else {
                valueParam = $("#editProductAttributeRealValueSize").val();
            }
        }
        else if($("#ddlEditProductAttributeType").val() === "color") {
            valueParam = $("#editProductAttributeColorRealValue").val();
        }
        else if($("#ddlEditProductAttributeType").val() === "other") {
            if(!checkEmpty($("#editProductAttributeOtherKey").val())) {
                swal("Atenção", "Por favor digite o nome do tipo de atributo do producto", "warning");
                return;
            }
            if(!checkEmpty($("#editProductAttributeOtherValue").val())) {
                swal("Atenção", "Por favor digite o valor do tipo de atributo do producto", "warning");
                return;
            }   
            typeParam = $("#editProductAttributeOtherKey").val();
            valueParam = $("#editProductAttributeOtherValue").val();
        }

        if(!checkEmpty($("#txtEditProductAttributeQuantity").val())) {
            swal("Atenção", "Por favor digite a quantidade de itens com este atributo", "warning");
            return;
        }

        $.ajax({
            type: "POST",
            url: "addeditproductattribute",
            async: false,
            data: { type: typeParam, value: valueParam, key: $("#txtEditProductAttributeKey").val(), additionalCost: $("#txtEditProductAttributeAddPrice").val(), purchaseOption: forSellParam, quantity: $("#txtEditProductAttributeQuantity").val() },
            success: function(response) { 
                if(response['statusCode'] != 200) {
                    swal("Erro", response['message'], "error");
                }
                else {
                    swal("Sucesso", response['message'], "success");

                    $("#editProductAttributeDisplayTable > tbody").empty();
                    response['array'].forEach(function(element) {
                        $("#editProductAttributeDisplayTable > tbody").append("<tr><td>" + element.type + "</td><td>" + element.value + "</td><td>"+element.key+"</td><td>" + element.quantity + "</td><td>" + element.additionalCost + "</td><td>" + element.purchaseOption + "</td><td><a class='btn btn-outline btn-primary' onclick='EditProductRemoveAttribute(\"" +element.id+ "\");'><i class='ti-trash'></i></a></td></tr>");
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(errorThrown === 'Bad Request') {
                    swal("Erro", 'Por favor insira seus dados correctamente', "error");
                }
                else if(errorThrown === 'Internal Server Error') {
                    swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                }
            }
        });

    })

    $('body').on('click', '#btnAddEditProductEditData', function(){
    
        if(!checkEmpty($("#txtEditProductLocation").val())) {
            swal("Erro", "Por favor digite a localização do producto", "error");
            return false;
        }

        if(!checkNumbersOnly($("#txtEditProductQuantity").val())) {
            swal("Erro", "Por favor digite a quantidade válida em estoque do producto", "error");
            return false;
        }

        if(!checkMoney($("#txtEditProductPrice").val())) {
            swal("Erro", "Por favor digite o preço do producto correctamente", "error");
            return false;
        }
        
        if(!checkEmpty($("#txtEditProductManufacture").val())) {
            swal("Erro", "Por favor digite o nome do fabricante do producto", "error");
            return false;
        }

        $.ajax({
            type: "PATCH",
            url: "editdata",
            async: false,
            data: { location: $("#txtEditProductLocation").val(), price: $("#txtEditProductPrice").val(), quantity: $("#txtEditProductQuantity").val(), manufacture: $("#txtEditProductManufacture").val() },
            success: function(response) { 
                if(response['statusCode'] != 200) {
                    swal("Erro", response['message'], "error");
                }
                else {
                    window.location.href = response['message'];   
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(errorThrown === 'Bad Request') {
                    swal("Erro", 'Por favor insira seus dados correctamente', "error");
                }
                else if(errorThrown === 'Internal Server Error') {
                    swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                }
            }
        });
    });

    $('body').on('click', '#btnAddEditProductEditGeneral', function(){
        
        if(!checkEmpty($("#txtEditProductProductName").val())) {
            swal("Erro", "Por favor digite o nome do producto correctamente", "error");
            return false;
        }

        if(!checkEmpty($("#txtEditProductMetaTagTitle").val())) {
            swal("Erro", "Por favor digite um titulo para o producto.", "error");
            return false;
        }

        if(!checkEmpty($("#txtEditProductMetaTagDescription").val())) {
            swal("Erro", "Por favor digite uma descrição tag para ajudar o cliente a entender do que se trata.", "error");
            return false;
        }

        if(!checkEmpty($("#txtEditProductDescription").val())) {
            swal("Erro", "Por favor digite uma descrição para ajudar o cliente a entender o producto", "error");
            return false;
        }

        $.ajax({
            type: "PATCH",
            url: "editgeneral",
            async: false,
            data: { productName: $("#txtEditProductProductName").val(), titleTag: $("#txtEditProductMetaTagTitle").val(), descriptionTag: $("#txtEditProductMetaTagDescription").val(), description: $("#txtEditProductDescription").val(), keywordsTag: $("#txtEditProductMetaTagKeywords").val() },
            success: function(response) { 
                if(response['statusCode'] != 200) {
                    swal("Erro", response['message'], "error");
                }
                else {
                    window.location.href = response['message'];   
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(errorThrown === 'Bad Request') {
                    swal("Erro", 'Por favor insira seus dados correctamente', "error");
                }
                else if(errorThrown === 'Internal Server Error') {
                    swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                }
            }
        });
    });
    /**
     * End Edit Product
     */
})

function EditProductRemoveAttribute(attributeIDParam) {
    swal({   
            title: "Tem a certeza?",   
            text: "Removendo o attribute, o mesmo deixará de fazer parte do producto",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Sim, remova!",   
            cancelButtonText: "Não, cancela!",   
            closeOnConfirm: false,   
            closeOnCancel: false }, 
            function(isConfirm){   
                if (isConfirm) {  
                    $.ajax({
                        type: "PATCH",
                        url: "removeattribute",
                        async: false,
                        data: { attributeID: attributeIDParam },
                        success: function(response) { 
                            if(response['statusCode'] != 200) {
                                swal("Atenção", response['message'], "warning");
                            }
                            else {
                                swal("Sucesso", response['message'], "success");

                                $("#editProductAttributeDisplayTable > tbody").empty();
                                response['array'].forEach(function(element) {
                                    $("#editProductAttributeDisplayTable > tbody").append("<tr><td>" + element.type + "</td><td>" + element.value + "</td><td>"+element.key+"</td><td>"+element.key+"</td><td>" + element.quantity + "</td><td>" + element.additionalCost + "</td><td>" + element.purchaseOption + "</td><td><a class='btn btn-outline btn-primary' onclick='EditProductRemoveAttribute(\"" +element.id+ "\");'><i class='ti-trash'></i></a></td></tr>");
                                });
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            if(errorThrown === 'Bad Request') {
                                swal("Erro", 'Por favor insira seus dados correctamente', "error");
                            }
                            else if(errorThrown === 'Internal Server Error') {
                                swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                            }
                        }
                    });
                }
                else {
                    swal("Cancelado", "Operação cancelada com sucesso", "success");
                }
        })
}

function EditProductRemoveImage(imgIDParam) {
    swal({   
            title: "Tem a certeza?",   
            text: "Removendo essa imagem, a mesma já não fará parte das imagens exibidas na loja online para este producto",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Sim, remova!",   
            cancelButtonText: "Não, cancela!",   
            closeOnConfirm: false,   
            closeOnCancel: false }, 
            function(isConfirm){   
                if (isConfirm) {  
                    $.ajax({
                        type: "PATCH",
                        url: "removeimage",
                        async: false,
                        data: { imgID: imgIDParam },
                        success: function(response) { 
                            if(response['statusCode'] != 200) {
                                swal("Atenção", response['message'], "warning");
                            }
                            else {
                                swal("Sucesso", response['message'], "success");

                                $("#EditProductImagesTable > tbody").empty();
                                response['array'].forEach(function(element) {
                                    $("#EditProductImagesTable > tbody").append("<tr><td class='text-center'><img src='" + element.path + "' width='100' alt='' class='img-thumbnail img-responsive'></td><td>" + "<input type='text' value='" + element.path +"' disabled name='txtImageUrl' class='form-control'></td><td><ul style='list-style-type: none;'><li style='display: block; padding: 1%'><input type='text' value='" +element.position + "' id='txtEditProductIamgesSortOrder"+element.position+"' class='form-control'></li><li style='display: block; padding: 1%'><button class='btn btn-sm btn-outline btn-info' type='button' onclick='EditProductChangeImagePosition(\"" +element.position+ "\",\"" + element.name + "\");'><i class='ti-save'></i></button></li></ul></td><td class='text-center'><button class='btn btn-sm btn-outline btn-danger' type='button' onclick='EditProductRemoveImage(\"" +element.name+ "\");'><i class='ti-trash'></i></button></td></tr>");
                                });
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            if(errorThrown === 'Bad Request') {
                                swal("Erro", 'Por favor insira seus dados correctamente', "error");
                            }
                            else if(errorThrown === 'Internal Server Error') {
                                swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                            }
                        }
                    });
                }
                else {
                    swal("Cancelado", "Operação cancelada com sucesso", "success");
                }
        })   
}
    
function EditProductChangeImagePosition(positionToChangeParam, imageIDParam) {
    
    $.ajax({
        type: "PATCH",
        url: "changeimgposition",
        async: false,
        data: { position: positionToChangeParam, imageID: imageIDParam, positionToGo: $("#txtEditProductIamgesSortOrder"+positionToChangeParam).val()},
        success: function(response) { 
            if(response['statusCode'] != 200) {
                swal("Erro", response['message'], "error");
            }
            else {
                swal("Sucesso", response['message'], "success");

                $("#EditProductImagesTable > tbody").empty();
                response['array'].forEach(function(element) {
                    $("#EditProductImagesTable > tbody").append("<tr><td class='text-center'><img src='" + element.path + "' width='100' alt='' class='img-thumbnail img-responsive'></td><td>" + "<input type='text' value='" + element.path +"' disabled name='txtImageUrl' class='form-control'></td><td><ul style='list-style-type: none;'><li style='display: block; padding: 1%'><input type='text' value='" +element.position + "' id='txtEditProductIamgesSortOrder"+element.position+"'class='form-control'></li><li style='display: block; padding: 1%'><button class='btn btn-sm btn-outline btn-info' type='button' onclick='EditProductChangeImagePosition(\"" +element.position+ "\",\"" + element.name + "\");'><i class='ti-save'></i></button></li></ul></td><td class='text-center'><button class='btn btn-sm btn-outline btn-danger' type='button' onclick='EditProductRemoveImage(\"" +element.name+ "\");'><i class='ti-trash'></i></button></td></tr>");
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(errorThrown === 'Bad Request') {
                swal("Erro", 'Por favor insira seus dados correctamente', "error");
            }
            else if(errorThrown === 'Internal Server Error') {
                swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
            }
        }
    });
}

function RemoveAttribute(idParam) {
    $.ajax({
        type: "PATCH",
        url: "removeproductattribute",
        async: false,
        data: { id: idParam },
        success: function(response) { 
            if(response['statusCode'] != 200) {
                swal("Erro", response['message'], "error");
            }
            else {
                swal("Sucesso", response['message'], "success");

                $("#newProductAttributeDisplayTable > tbody").empty();
                response['array'].forEach(function(element) {
                    $("#newProductAttributeDisplayTable > tbody").append("<tr><td>" + element.type + "</td><td>" + element.value + "</td><td>" + element.quantity + "</td><td>" + element.additionalCost + "</td><td>" + element.purchaseOption + "</td><td><a class='btn btn-outline btn-primary' onclick='RemoveAttribute(\"" +element.id+ "\");'><i class='ti-trash'></i></a></td></tr>");
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(errorThrown === 'Bad Request') {
                swal("Erro", 'Por favor insira seus dados correctamente', "error");
            }
            else if(errorThrown === 'Internal Server Error') {
                swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
            }
        }
    });
}

function RemoveProduct(productIDParam) {
    swal({   
            title: "Tem a certeza?",   
            text: "Removendo este producto o mesmo deixará de fazer parte da sua loja online",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Sim, remova!",   
            cancelButtonText: "Não, cancela!",   
            closeOnConfirm: false,   
            closeOnCancel: false }, 
            function(isConfirm){   
                if (isConfirm) {  
                    $.ajax({
                        type: "PATCH",
                        url: "/products/remove",
                        async: false,
                        data: { productID: productIDParam },
                        success: function(response) { 
                            if(response['statusCode'] != 200) {
                                swal("Atenção", response['message'], "warning");
                            }
                            else {
                                window.location.href = response['message'];
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            if(errorThrown === 'Bad Request') {
                                swal("Erro", 'Por favor insira seus dados correctamente', "error");
                            }
                            else if(errorThrown === 'Internal Server Error') {
                                swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                            }
                        }
                    });
                }
                else {
                    swal("Cancelado", "Operação cancelada com sucesso", "success");
                }
        })   
}

/*Validation Functions*/
function checkEmpty(prodName) {
    return /([^\s])/.test(prodName)
}

function checkNumbersOnly(valueParam) {
    return /^-?\d+\.?\d*$/.test(valueParam);
}

function checkMoney(i) {
    var regex = /^[0-9][0-9.]*[0-9]\,?[0-9]{0,2}$/i;
    return regex.test(i);
}
/*end validation functions*/
