"use strict";
$(document).ready(function(){
    $("#loginSpinner").css('visibility', 'hidden');

    $("#cmdLogin").on('click', function() {
        
        $("#loginSpinner").css('visibility', 'visible');
        var intv = setInterval(ProcessLogin, 1000);
        function ProcessLogin() {
            $.ajax({
                type: "POST",
                url: "authUser",
                async: false,
                data: { email: $("#email").val(), password: $("#password").val() },
                success: function(response) { 
                    if(response['statusCode'] != 200) {
                        clearInterval(intv);
                        $("#loginSpinner").css('visibility', 'hidden');
                        swal("Erro", response['message'], "error");
                    }
                    else {
                        clearInterval(intv);
                        $("#loginSpinner").css('visibility', 'hidden');
                        window.location.href = response['message'];
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    clearInterval(intv);
                    $("#loginSpinner").css('visibility', 'hidden');
                    if(errorThrown === 'Bad Request') {
                        swal("Erro", 'Por favor insira seus dados correctamente', "error");
                    }
                    else if(errorThrown === 'Internal Server Error') {
                        swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                    }
                }
            });
        }
    })

    $("#cmdChangePassword").on('click', function() {
        
        var loc = window.location;
        var pathName = loc.pathname.substring(loc.pathname.length, loc.pathname.lastIndexOf('/')+1);
        
        $("#loginSpinner").css('visibility', 'visible');
        var intv = setInterval(ChangePassword, 1000);
        function ChangePassword() {
            $.ajax({
                type: "POST",
                url: "changepswd",
                async: false,
                data: { email: pathName, newpassword: $("#passwordchange").val(), passwordconf: $("#passwordchangeconfirmation").val() },
                success: function(response) { 
                    if(response['statusCode'] != 200) {
                        clearInterval(intv);
                        $("#loginSpinner").css('visibility', 'hidden');
                        swal("Erro", response['message'], "error");
                    }
                    else {
                        clearInterval(intv);
                        $("#loginSpinner").css('visibility', 'hidden');
                        window.location.href = "/" + response['message'];
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    clearInterval(intv);
                    $("#loginSpinner").css('visibility', 'hidden');
                    if(errorThrown === 'Bad Request') {
                        swal("Erro", 'Por favor insira seus dados correctamente', "error");
                    }
                    else if(errorThrown === 'Internal Server Error') {
                        swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                    }
                }
            });
        }
    })
})
