$(document).ready(function(){
    Dropzone.options.myAwesomeDropzone=!1;
    Dropzone.autoDiscover=!1;
    var myDropzone = $("#newProductFiles").dropzone({
        url:"/uploadimg",
        paramName:"file",
        maxFiles: 10,
        acceptedFiles: "image/*",
        addRemoveLinks: true,
        dictRemoveFile: "Remover",
        maxFilesize:2,
        maxThumbnailFilesize:.5,
        dictDefaultMessage:"<i class='icon-dz fa fa-files-o'></i>Arraste as imagens para essa area ou clique para selecionar",
        init: function() {
            this.on("removedfile", function(fileParam) {
                $.post("removeimg", { file: fileParam.name } );
            });
            this.on("addedfile", function(file) { 
                numberOfFiles = this.files.length;
            });
        },

    })
});