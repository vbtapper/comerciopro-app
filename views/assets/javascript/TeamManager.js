"use strict";
$(document).ready(function(){

    $("#newMemberSpinner").css('visibility', 'hidden');

    var oTable = $('#team-table').dataTable({
        ajax :{
            url : "/getteams",
            type: "GET"
        },
        "serverSide": false,
        "processing": true,
        "lengthChange":!1,
        "pageLength":5,
        "colReorder":!0,
        "aoColumns": [{
            "mData" : "name"
        },
        {   
            "mData" : "email" 
        },
        {
            "mData" : "role"
        },
        {
            "mData" : "active"
        },
        {
            "mData": null,
            "bSortable": false,
            "mRender": function(data, type, full) {
                if(data['active']) {
                    return "<a class='btn btn-info btn-sm' onclick='DisableMember(\"" +data['id']+ "\");'> Disactivar" + "</a>";
                }
                else {
                    return "<a class='btn btn-info btn-sm' onclick='EnableMember(\"" +data['id']+ "\");'> Activar" + "</a>";
                }      
            }
        },
        {
            "mData": null,
            "bSortable": false,
            "mRender": function(data, type, full) {
                return "<a class='btn btn-info btn-sm' onclick='RemoveMember(\"" +data['id']+ "\");'> Remover" + "</a>";
            }
        }],
        "language": {
                "lengthMenu": "Mostrando _MENU_ productos por página",
                "zeroRecords": "Não há membros registrados",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "Não há membros registrados",
                "infoFiltered": "(filtrados de _MAX_ productos)",
                "search": "",
                "paginate": {
                    "previous": "Anterior",
                    "next" : "Seguinte"
                },
                "loading": "carregando..."
        },
        "scroller": {
            loadingIndicator: true
        },
        "searching": false
    });

    $("#btnSendAddNewMember").on('click', function() {
        
        if(!ValidateName($("#txtFirstNameNewMember").val())) {
            swal("Erro", "Por favor digite o primeiro nome correctamente", "error");
            return;
        }

        if(!ValidateName($("#txtLastNameNewMember").val())) {
            swal("Erro", "Por favor digite o ultimo nome correctamente", "error");
            return;
        }

        if(!ValidateEmail($("#txtEmailNewMember").val())) {
            swal("Erro", "Por favor digite o e-mail correctamente", "error");
            return;
        }

        if($("#cboRoles").val() === 'none') {
            swal("Erro", "Por favor escolha a função do novo membro", "error");
            return;
        }

        $("#newMemberSpinner").css('visibility', 'visible');
        var intv = setInterval(AddNewMember, 1000);
        function AddNewMember() {
            $.ajax({
                type: "POST",
                url: "addnewmember",
                async: false,
                data: { email: $("#txtEmailNewMember").val(), firstname: $("#txtFirstNameNewMember").val(), lastname: $("#txtLastNameNewMember").val(), role: $("#cboRoles").val() },
                success: function(response) { 
                    if(response['statusCode'] != 200) {
                        clearInterval(intv);
                        $("#newMemberSpinner").css('visibility', 'hidden');
                        swal("Erro", response['message'], "error");
                    }
                    else {
                        clearInterval(intv);
                        $("#newMemberSpinner").css('visibility', 'hidden');
                        window.location.href = response['message'];
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    clearInterval(intv);
                    $("#newMemberSpinner").css('visibility', 'hidden');
                    if(errorThrown === 'Bad Request') {
                        swal("Erro", 'Por favor insira seus dados correctamente', "error");
                    }
                    else if(errorThrown === 'Internal Server Error') {
                        swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                    }
                }
            });
        }
    })
    
})

function RemoveMember(memberID) {
    swal({   
        title: "Tem a certeza?",   
        text: "Esse membro perderá o acesso a sua conta e o seu historico será removido.",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Sim, remova!",   
        cancelButtonText: "Não, cancela!",   
        closeOnConfirm: false,   
        closeOnCancel: false }, 
        function(isConfirm){   
            if (isConfirm) {     
                $.ajax({
                    type: "PATCH",
                    url: "removemember",
                    async: false,
                    data: { memberid: memberID },
                    success: function(response) { 
                        if(response['statusCode'] != 200) {
                            swal("Erro", response['message'], "error");
                        }
                        else {
                            window.location.href = response['message'];
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                    }
                });
            }
            else {
                swal("Cancelado", "Operação cancelada com sucesso", "success");
            }
        });
}

function EnableMember(memberID) {
    swal({   
        title: "Tem a certeza?",   
        text: "Activando esse membro, o mesmo voltará a ter acesso aos dados",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Sim, active!",   
        cancelButtonText: "Não, cancela!",   
        closeOnConfirm: false,   
        closeOnCancel: false }, 
        function(isConfirm){   
            if (isConfirm) {     
                $.ajax({
                    type: "PATCH",
                    url: "enablemember",
                    async: false,
                    data: { memberid: memberID },
                    success: function(response) { 
                        if(response['statusCode'] != 200) {
                            swal("Erro", response['message'], "error");
                        }
                        else {
                            window.location.href = response['message'];
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                    }
                });
            }
            else {
                swal("Cancelado", "Operação cancelada com sucesso", "success");
            } 
        });
}

function DisableMember(memberID) {
    swal({   
        title: "Tem a certeza?",   
        text: "Desativando esse membro, o mesmo perderá a ter acesso a conta e aos dados",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Sim, desactive!",   
        cancelButtonText: "Não, cancela!",   
        closeOnConfirm: false,   
        closeOnCancel: false }, 
        function(isConfirm){   
            if (isConfirm) {     
                $.ajax({
                    type: "PATCH",
                    url: "disablemember",
                    async: false,
                    data: { memberid: memberID },
                    success: function(response) { 
                        if(response['statusCode'] != 200) {
                            swal("Erro", response['message'], "error");
                        }
                        else {
                            window.location.href = response['message'];
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        swal("Erro", 'Ocorreu um erro interno. Por favor contacte o administrador do sistema.', "error");
                    }
                });
            }
            else {
                swal("Cancelado", "Operação cancelada com sucesso", "success");
            }
        });
}

function ValidateName(name) {
    return /^[a-zA-Z]*[a-zA-Z]+[a-zA-Z]*$/.test(name);
}

function ValidateEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}