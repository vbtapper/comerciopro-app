const config            = require('../config.js');
const Client            = require('node-rest-client').Client;

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

module.exports = {
    RenderDashboardPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('login', {}, { layout: 'authLayout' });
        }
        else {
            var loggedUser = {
                role: request.session.scope,
                name: request.session.userName
            }

            if(request.session.scope === 'OWNER') {
                return reply.view('dashboard', loggedUser, { layout: 'ownerLayout' });
            }
            else if(request.session.scope === 'FINANCES') {
                return reply.view('dashboard', loggedUser, { layout: 'financesLayout' });
            }
            else if(request.session.scope === 'OPERATOR') {
                return reply.view('dashboard', loggedUser, { layout: 'operatorLayout' });
            }
        }
    }
}