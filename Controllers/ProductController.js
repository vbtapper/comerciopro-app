const fs                          = require('fs');
const Path                        = require('path');
const HttpRequest                 = require('request');
const Client                      = require('node-rest-client').Client;
const Q                           = require('q');
const config                      = require('../config.js');
const forEach                     = require('async-foreach').forEach;
const async                       = require("async");
const randtoken                   = require('rand-token').generator();

var AttributesArray = new Array();
var SelectedProduct = null;
var EditProductArrayImagesToRemove = new Array()

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function UploadToMedia(fileParam) {
    
    var Uploaddeferred = Q.defer();

    var formData = {
        file: fs.createReadStream(fileParam),
    };
   
    HttpRequest.post({url:config.media.endpoint + '/post', formData: formData}, function optionalCallback(err, httpResponse, body) {
        var obj = JSON.parse(body); 
        if(obj.statusCode != 200) {
            Uploaddeferred.reject(obj);
        }
        else {
            Uploaddeferred.resolve(obj);
        }
    }).auth(null, null, true, config.media.accessKey);

     return Uploaddeferred.promise;
}
    
function RemoveImagesFromPath(folderName) {
    var folderPath = Path.join(__dirname, "../repositories/" + folderName);
    fs.readdir(folderPath, function(err, filenames) {
        forEach(filenames, function(item, index, arr) {
            fs.unlinkSync(folderPath + "/" + item)
        });
    })
}

function RemoveImagesFromMedia(imageID) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + config.media.accessKey
        }
    };
    client.patch(config.media.endpoint + "/remove/" + imageID, args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;  
}

function AddProductRequest(teamIDParam, productNameParam, titleTagParam, descriptionTagParam, keywordsTagParam, descriptionParam, modelParam, categoryParam, locationParam, quantityParam, priceParam, manufactureParam, brandParam, imagesParam, accessTokenParam) {

    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            teamID: teamIDParam,
            productName: productNameParam,
            titleTag: titleTagParam,
            descriptionTag: descriptionTagParam,
            keywordsTag: keywordsTagParam,
            description: descriptionParam,
            model: modelParam,
            category: categoryParam,
            brand: brandParam,
            location: locationParam,
            quantity: quantityParam,
            price: priceParam,
            manufacture: manufactureParam,
            images: imagesParam,
            attributes: AttributesArray
        }
    };
    client.post(config.API.endpoint + "/addproduct", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function GetProductsRequest(accessTokenParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        parameters: { 
            sessiontoken: accessTokenParam
        }
    };

    client.get(config.API.endpoint + "/getproducts", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function GetProductDetailRequest(productIDParam, accessTokenParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        parameters: { 
            productID: productIDParam
        }
    };

    client.get(config.API.endpoint + "/getproductdetail", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;  
}

function PublishProductRequest(accessTokenParam, productIDParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            productID: productIDParam
        }
    };

    client.patch(config.API.endpoint + "/publishproduct", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function UnPublishProductRequest(accessTokenParam, productIDParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            productID: productIDParam
        }
    };

    client.patch(config.API.endpoint + "/unpublishproduct", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function AddNewImageProductRequest(accessTokenParam, productIDParam, imgObjParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            productID: productIDParam,
            imageObj: imgObjParam
        }
    };

    client.patch(config.API.endpoint + "/addimageproduct", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function RemoveImageProductRequest(accessTokenParam, productIDParam, imageIDParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            productID: productIDParam,
            imageID: imageIDParam
        }
    };

    client.patch(config.API.endpoint + "/removeimageproduct", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function ChangePositionImageProductRequest(accessTokenParam, productIDParam, positionParam, positionToGoParam) {
    
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            productID: productIDParam,
            position: positionParam,
            positionToGo: positionToGoParam
        }
    };

    client.patch(config.API.endpoint + "/changepositionimageproduct", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function RemoveAttributeRequest(accessTokenParam, productIDParam, attributeIDParam) {
    
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            productID: productIDParam,
            attributeID: attributeIDParam 
        }
    };

    client.patch(config.API.endpoint + "/removeattributeproduct", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function AddAttributeRequest(accessTokenParam, productIDParam, attributeObjParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            id: attributeObjParam.id,
            type: attributeObjParam.type,
            value: attributeObjParam.value,
            key: attributeObjParam.key,
            quantity: attributeObjParam.quantity,
            additionalCost: attributeObjParam.additionalCost,
            purchaseOption: attributeObjParam.purchaseOption,
            productID: productIDParam
        }
    };

    client.patch(config.API.endpoint + "/addattributeproduct", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function EditProductDataRequest(accessTokenParam, productIDParam, newDataObj) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            location: newDataObj.location,
            price: newDataObj.price,
            quantity: newDataObj.quantity,
            vendor: newDataObj.vendor,
            productID: productIDParam
        }
    };

    client.patch(config.API.endpoint + "/editproductdata", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function EditProductGeneralRequest(accessTokenParam, productIDParam, newDataObj) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            productName: newDataObj.productName,
            titleTag: newDataObj.titleTag,
            descriptionTag: newDataObj.descriptionTag,
            keywordsTag: newDataObj.keywordsTag,
            description: newDataObj.description,
            productID: productIDParam
        }
    };

    client.patch(config.API.endpoint + "/editproductgeneral", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function RemoveProductRequest(accessTokenParam, productIDParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            productID: productIDParam
        }
    };

    client.patch(config.API.endpoint + "/removeproduct", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

module.exports = {
    RenderNewProductPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('login', {}, { layout: 'authLayout' });
        }
        else {
            var loggedUser = {
                role: request.session.scope,
                name: request.session.userName
            }

            if(request.session.scope === 'OWNER') {
                return reply.view('newproduct', loggedUser, { layout: 'ownerLayout' });
            }
            else {
                return reply.redirect(new Error('404'));
            }
        }
    },

    RenderListProductsPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(config.Base.devURL, {}, { layout: 'authLayout' });
        }
        else {
            var loggedUser = {
                role: request.session.scope,
                name: request.session.userName
            }

            if(request.session.scope === 'OWNER') {
                return reply.view('listproducts', loggedUser, { layout: 'ownerLayout' });
            }
            else {
                return reply.redirect(new Error('404'));
            }
        }
    },

    RenderViewProductPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(config.Base.devURL, {}, { layout: 'authLayout' });
        }
        else {
            GetProductDetailRequest(request.params.productID, request.session.sessiontoken)
            .then(FoundProduct => {
                var dataToSend = {
                    role: request.session.scope,
                    name: request.session.userName,
                    product: FoundProduct.product
                }
                if(request.session.scope === 'OWNER') {
                    return reply.view('viewproduct', dataToSend, { layout: 'ownerLayout' });
                }
                else {
                    return reply.redirect(new Error('Not Found'));
                }
            })
            .catch(error => {
                console.log(error)
                return reply.view(new Error('Internal Error'));
            })
        }
    },

    RenderEditProductPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(config.Base.devURL, {}, { layout: 'authLayout' });
        }
        else {
            GetProductDetailRequest(request.params.productID, request.session.sessiontoken)
            .then(FoundProduct => {
                
                SelectedProduct = FoundProduct.product;

                var dataToSend = {
                    role: request.session.scope,
                    name: request.session.userName,
                    product: FoundProduct.product
                }
                if(request.session.scope === 'OWNER') {
                    return reply.view('editproduct', dataToSend, { layout: 'ownerLayout' });
                }
                else {
                    return reply.redirect(new Error('Not Found'));
                }
            })
            .catch(error => {
                console.log(error)
                return reply.view(new Error('Internal Error'));
            })
        }
    },

    UploadImagesProcessor: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            try {
                var data = request.payload;
                var name = data.file.hapi.filename;
                var path = Path.join(__dirname, "../repositories/" + request.session.repname + "/" + name);
                var file = fs.createWriteStream(path);
                data.file.pipe(file);
                data.file.on('end', function (err) { 
                    return reply('done');
                })
            }
            catch(error) {
                console.log(error)
                return reply('done')
            }
        }
    },

    RemoveImagesProcessor: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            var name = request.payload.file;
            var path = Path.join(__dirname, "../repositories/" + request.session.repname + "/" + name);
            var file = fs.createWriteStream(path);
            fs.unlinkSync(path);
            
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success'
            }
            return reply(infoResponse)
        }
    },

    AddProduct: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            var imagesArray = [];
            function PushToArrayImages(jsonObjParam) {
                imagesArray.push(jsonObjParam)
            }

            var folderPath = Path.join(__dirname, "../repositories/" + request.session.repname);
            fs.readdir(folderPath, function(err, filenames) {
                if (err) {
                    var infoResponse = {
                        statusCode: 403,
                        error: 'Request Error',
                        message: 'Erro no upload das imagens, por favor reve as imagens e tenta novamente'
                    }
                    return reply(infoResponse)
                }
                var imgPosition = 0;
                async.forEach(filenames, function (element, callback){ 
                    var realPath = folderPath + "/" + element 
                    UploadToMedia(realPath)
                    .then(UploadImageObject => {
                        var nameParam = UploadImageObject['fileName'];
                        var urlParam = config.media.endpoint + "/assets/image/" + nameParam;
                        var jsonObjImg = {
                            name: nameParam,
                            path: urlParam,
                            position: imgPosition
                        }
                        imgPosition = imgPosition + 1;
                        PushToArrayImages(jsonObjImg); 
                        callback()
                    })
                    .catch(error => {
                        RemoveImagesFromPath(request.session.repname)
                        imagesArray.forEach(function(element) {
                            RemoveImagesFromMedia(element.name);
                        })
                        var infoResponse = {
                            statusCode: 403,
                            error: 'Request Error',
                            message: 'Erro durante o upload das imagens, por favor tente mais tarde ou contacte o administrador'
                        }
                        return reply(infoResponse)
                    })
                }, function(err) {
                    AddProductRequest(request.session.teamID, request.payload.productName, request.payload.titleTag, request.payload.descriptionTag, request.payload.keywordsTag, request.payload.description, request.payload.model, request.payload.category, request.payload.location, request.payload.quantity, request.payload.price, request.payload.manufacture, request.payload.brand, imagesArray, request.session.sessiontoken)
                    .then(AddedObject => {
                        AttributesArray = [];
                        var infoResponse = {
                            statusCode: 200,
                            error: null,
                            message: 'products/' + AddedObject.publicID
                        }
                        RemoveImagesFromPath(request.session.repname)
                        return reply(infoResponse)
                    })
                    .catch(error => {
                        console.log(error)
                        var infoResponse = {
                            statusCode: 403,
                            error: 'Request Error',
                            message: 'Aconteceu um erro interno, por favor tente mais tarde ou contacte o administrador'
                        }
                        RemoveImagesFromPath(request.session.repname)
                        imagesArray.forEach(function(element) {
                            RemoveImagesFromMedia(element.name)
                        })
                        return reply(infoResponse)
                    })
                });
            });   
        }   
    },

    GetProducts: function(request, reply) {    
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            GetProductsRequest(request.session.sessiontoken)
            .then(FoundProducts => {
                var data = {
                    data: FoundProducts.data
                }
                return reply(data);
            })
            .catch(error => {
                console.log(error)
                return reply({
                    data: []
                })
            })
        }
    },

    AddProductAttribute: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            var newAttrb = {
                id: randtoken.generate(10, config.utils.randomAttr),
                type: request.payload.type,
                value: request.payload.value,
                key: request.payload.key,
                quantity: request.payload.quantity,
                additionalCost: request.payload.additionalCost,
                purchaseOption: request.payload.purchaseOption
            }

            AttributesArray.push(newAttrb)
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                array: AttributesArray
            }
            return reply(infoResponse)
        }
    },

    RemoveProductAttribute: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            AttributesArray = AttributesArray.filter(obj => obj.id != request.payload.id)
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                array: AttributesArray
            }
            return reply(infoResponse)
        }
    },

    PublishProduct: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            PublishProductRequest(request.session.sessiontoken, request.payload.productID)
            .then(UpdatedProduct => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: UpdatedProduct.publicID
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error);
                return reply({
                    statusCode: 500,
                    error: 'Internal Error',
                    message: error.message
                })
            })
        }
    },

    UnPublishProduct: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            UnPublishProductRequest(request.session.sessiontoken, request.payload.productID)
            .then(UpdatedProduct => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: UpdatedProduct.publicID
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error);
                return reply({
                    statusCode: 500,
                    error: 'Internal Error',
                    message: error.message
                })
            })
        }
    },

    EditProductRemoveImage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            if(SelectedProduct.images.length == 1) {
                var errorResponse = {
                    statusCode: 403,
                    error: 'Request Error',
                    message: 'Não é possivel remover todas as imagens do producto',
                }
                return reply(errorResponse)
            }
            else {
                RemoveImageProductRequest(request.session.sessiontoken, SelectedProduct.publicID, request.payload.imgID)
                .then(RemovedImageObject => {

                    RemoveImagesFromMedia(request.payload.imgID);
                    SelectedProduct.images = RemovedImageObject.images

                    var infoResponse = {
                        statusCode: 200,
                        error: null,
                        message: 'Imagem removida com sucesso',
                        array: RemovedImageObject.images
                    }
                    return reply(infoResponse)
                })
                .catch(error => {
                    console.log(error)
                    var infoResponse = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'Ocorreu um erro interno, por favor tente mais tarde'
                    }
                    return reply(infoResponse)
                })
                
            }
        }
    },

    EditProductUploadImagesProcessor: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            try {
                var data = request.payload;
                var name = data.file.hapi.filename;
                var path = Path.join(__dirname, "../repositories/" + request.session.repname + "/" + name);
                var file = fs.createWriteStream(path);
                data.file.pipe(file);
                data.file.on('end', function (err) { 
                    var maxPosition = SelectedProduct.images[0].position;
                    async.forEach(SelectedProduct.images, function (element, callback){
                        if(element.position > maxPosition) {
                            maxPosition = element.position
                        }
                        callback();
                    },function(err) {
                        UploadToMedia(path)
                        .then(UploadImageObject => {
                            var nameParam = UploadImageObject['fileName'];
                            var urlParam = config.media.endpoint + "/assets/image/" + nameParam;
                            var jsonObjImg = {
                                name: nameParam,
                                path: urlParam,
                                position: maxPosition + 1
                            }
                            AddNewImageProductRequest(request.session.sessiontoken, SelectedProduct.publicID, jsonObjImg)
                            .then(ServerResponse => {
                                SelectedProduct.images = ServerResponse.images
                                RemoveImagesFromPath(request.session.repname)
                                return reply({
                                    statusCode: 200,
                                    error: null,
                                    message: 'success',
                                    array: ServerResponse.images
                                });  
                            }) 
                            .catch(error => {
                                RemoveImagesFromMedia(jsonObjImg.name);
                                RemoveImagesFromPath(request.session.repname)
                            })
                        })
                        .catch(error => {
                            RemoveImagesFromPath(request.session.repname)
                            return reply('done')
                        }) 
                    })
                    
                })
            }
            catch(error) {
                console.log(error)
                return reply('done')
            }
        }
    },

    EditProductChangeImagePosition: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            ChangePositionImageProductRequest(request.session.sessiontoken, SelectedProduct.publicID, request.payload.position, request.payload.positionToGo)
            .then(PositionsChanged => {
                console.log(PositionsChanged)
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'image position changed',
                    array: PositionsChanged.images
                }
                return reply(infoResponse)
            })
            .catch(error => {
                var infoResponse = {
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Ocorreu um erro interno. Por favor tente mais tarde'
                }
                return reply(infoResponse)
            })
        }
    },

    EditProductRemoveAttribute: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            RemoveAttributeRequest(request.session.sessiontoken, SelectedProduct.publicID, request.payload.attributeID)
            .then(AtributesChanged => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'Atribute removido com sucesso',
                    array: AtributesChanged.attributes
                }
                return reply(infoResponse)
            })
            .catch(error => {
                var infoResponse = {
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Ocorreu um erro interno. Por favor tente mais tarde'
                }
                return reply(infoResponse)
            })
        }
    },

    EditProductAddNewAttribute: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            var newAttrb = {
                id: randtoken.generate(10, config.utils.randomAttr),
                type: request.payload.type,
                value: request.payload.value,
                key: request.payload.key,
                quantity: request.payload.quantity,
                additionalCost: request.payload.additionalCost,
                purchaseOption: request.payload.purchaseOption
            }

            AddAttributeRequest(request.session.sessiontoken, SelectedProduct.publicID, newAttrb)
            .then(newAttributeObj => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'Atribute adicionado com sucesso.',
                    array: newAttributeObj.attributes
                }
                return reply(infoResponse)
            })
            .catch(error => {
                return reply({
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Ocorreu um erro interno. Por favor tente mais tarde.'
                })
            })
        }
    },

    EditProductEditData: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            var editedData = {
                location: request.payload.location,
                price: request.payload.price,
                quantity: request.payload.quantity,
                vendor: request.payload.manufacture,
            }

            EditProductDataRequest(request.session.sessiontoken, SelectedProduct.publicID, editedData)
            .then(newDataObj => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: '/products/' + newDataObj.publicID
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error)
                return reply({
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Ocorreu um erro interno. Por favor tente mais tarde.'
                })
            })
        }
    },

    EditProductEditGeneral: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            var editedGeneral = {
                productName: request.payload.productName,
                titleTag: request.payload.titleTag,
                descriptionTag: request.payload.descriptionTag,
                keywordsTag: request.payload.keywordsTag,
                description: request.payload.description,
            }

            EditProductGeneralRequest(request.session.sessiontoken, SelectedProduct.publicID, editedGeneral)
            .then(newDataObj => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: '/products/' + newDataObj.publicID
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error)
                return reply({
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Ocorreu um erro interno. Por favor tente mais tarde.'
                })
            })
        }
    },

    RemoveProduct: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error(400));
        }
        else {
            RemoveProductRequest(request.session.sessiontoken, request.payload.productID)
            .then(UpdatedProduct => {

                UpdatedProduct.images.forEach(function(element) {
                    RemoveImagesFromMedia(element.name);
                });

                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: '/products'
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error);
                return reply({
                    statusCode: 500,
                    error: 'Internal Error',
                    message: error.message
                })
            })
        }
    },
}