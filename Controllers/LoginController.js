const config            = require('../config.js');
const Client            = require('node-rest-client').Client;
const Q                 = require('q');
const fs                = require('fs');
const Path              = require('path');

var client = new Client();

function LoginRequest(emailParam, passwordParam) {
    var deferred = Q.defer();
    var args = {
        parameters: { 
            email : emailParam,
            password: passwordParam,
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        }
    };
    client.get(config.API.endpoint + "/memberlogin", args, function (ResponseData, response) {
        if(ResponseData['statusCode'] == 200) {
            deferred.resolve(ResponseData);
        }
        else {
            deferred.reject(ResponseData);
        }
    })
    
    return deferred.promise;
}

function ChangePasswordRequest(emailParam, newPasswordParam) {

    var deferred = Q.defer();
    var args = {
        data: { 
            email : emailParam,
            newpassword: newPasswordParam,
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        }
    };
    client.patch(config.API.endpoint + "/chpswd", args, function (ResponseData, response) {
        if(ResponseData['statusCode'] == 200) {
            deferred.resolve(ResponseData);
        }
        else {
            deferred.reject(ResponseData);
        }
    })
    
    return deferred.promise;
}
module.exports = {
    RenderLoginPage: function(request, reply) {
        return reply.view('login', {}, { layout: 'authLayout' });
    },

    Login: function(request, reply) {
        LoginRequest(request.payload.email, request.payload.password)
        .then(LoginResponse => {
            if(LoginResponse['message'] === 'not active') {
                var infoResponse = {
                    statusCode: 403,
                    error: 'Not Authorized',
                    message: 'Está conta encontrasse inativa por favor contacte o administrador da conta.'
                }
                return reply(infoResponse)
            }
            else {
                request.session.logged = true;
                request.session.scope = LoginResponse['scope'];
                request.session.teamID = LoginResponse['teamid'];
                request.session.sessiontoken = LoginResponse['sessiontoken'];
                request.session.userName = LoginResponse['name'];
                request.session.repname = LoginResponse['repName'];

                var pathFolder = Path.join(__dirname, "../repositories/" + LoginResponse['repName']);
                fs.mkdir(pathFolder, function() {});

                var newPath = 'dashboard'
                if(LoginResponse['firsttime']) {
                    newPath = 'ftlogin/' + request.payload.email;
                }
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: newPath
                }
                return reply(infoResponse)
            }
            
        })
        .catch(error => {
            if(error.statusCode == 400) {
                var infoResponse = {
                    statusCode: 403,
                    error: 'Bad Request',
                    message: 'E-mail ou Senha errada. Por favor reve seus dados e tente novamente.'
                }
                return reply(infoResponse)
            }
            else if(error.statusCode == 404) {
                var infoResponse = {
                    statusCode: 403,
                    error: 'Bad Request',
                    message: 'E-mail ou Senha errada. Por favor reve seus dados e tente novamente.'
                }
                return reply(infoResponse)
            }
            else if(error.statusCode == 500) {
                var infoResponse = {
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Ocorreu um erro interno, se o erro presistir, por favor contact o administrador do sistema'
                }
                return reply(infoResponse)
            }
        })
    },

    RenderFirstTimeLogin: function(request, reply) {
        return reply.view('firsttimelogin', {}, { layout: 'authLayout' });
    },

    ChangePassword: function(request, reply) {
        if(request.payload.newpassword !== request.payload.passwordconf) {
            var infoResponse = {
                statusCode: 400,
                error: 'Bad Request',
                message: 'As senhas devem ser iguais, por favor reve seus dados e tente novamente.'
            }
            return reply(infoResponse)
        }
        else {
            ChangePasswordRequest(request.payload.email, request.payload.newpassword)
            .then(Response => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'login'
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error)
                if(error.statusCode === 404 || error.statusCode === 500) {
                    var infoResponse = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'Ocorreu um erro interno. Por favor tente mais tarde.'
                    }
                    return reply(infoResponse)
                }
                else {
                    var infoResponse = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'Ocorreu um não identificado. Por favor contacte o administrador do sistema.'
                    }
                    return reply(infoResponse)
                }
            })
            
        }
    },

    LogOut: function(request, reply) {
        request.session = null;
        return reply.redirect('login', {}, { layout: 'authLayout' });
    }
}