const config            = require('../config.js');
const Client            = require('node-rest-client').Client;
const Q                 = require('q');

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function GetMembersRequest(accessTokenParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        parameters: { 
            sessiontoken: accessTokenParam
        }
    };

    client.get(config.API.endpoint + "/getmembers", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function ActivateMemberRequest(accessTokenParam, memberIDParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            memberID: memberIDParam
        }
    };

    client.patch(config.API.endpoint + "/actmember", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function DisactivateMemberRequest(accessTokenParam, memberIDParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            memberID: memberIDParam
        }
    };

    client.patch(config.API.endpoint + "/disactmember", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function RemoveMemberRequest(accessTokenParam, memberIDParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            memberID: memberIDParam
        }
    };

    client.patch(config.API.endpoint + "/removemember", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;     
}

function AddNewMemberRequest(emailParam, firstNameParam, lastNameParam, teamIDParam, scopeParam, accessTokenParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : 'Bearer ' + accessTokenParam
        },
        data: { 
            email: emailParam,
            firstname: firstNameParam,
            lastname: lastNameParam,
            teamID: teamIDParam,
            scope: scopeParam
        }
    };

    client.post(config.API.endpoint + "/addmember", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;  
}

module.exports = {
    RenderTeamPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('login', {}, { layout: 'authLayout' });
        }
        else {
            var loggedUser = {
                role: request.session.scope,
                name: request.session.userName
            }

            if(request.session.scope === 'OWNER') {
                return reply.view('team', loggedUser, { layout: 'ownerLayout' });
            }
            else {
                return reply.redirect(new Error('404'));
            }
        }
    },

    RenderAddNewMember: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('login', {}, { layout: 'authLayout' });
        }
        else {
            var loggedUser = {
                role: request.session.scope,
                name: request.session.userName
            }

            if(request.session.scope === 'OWNER') {
                return reply.view('newmember', loggedUser, { layout: 'ownerLayout' });
            }
            else {
                return reply.redirect(new Error('404'));
            }
        }
    },

    GetMembers: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            GetMembersRequest(request.session.sessiontoken)
            .then(FoundMembers => {
                var data = {
                    data: FoundMembers.data
                }
                return reply(data);
            })
            .catch(error => {
                console.log(error)
                return reply({
                    data: []
                })
            })
        }
    },

    EnableMember: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            ActivateMemberRequest(request.session.sessiontoken, request.payload.memberid)
            .then(DoneObject => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'team'
                }
                return reply(infoResponse)
            })
            .catch(error => {
                var infoResponse = {
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Occoreu um erro interno por favor tente mais tarde.'
                }
                return reply(infoResponse)
            })
        }
    },

    DisableMember: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            DisactivateMemberRequest(request.session.sessiontoken, request.payload.memberid)
            .then(DoneObject => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'team'
                }
                return reply(infoResponse)
            })
            .catch(error => {
                var infoResponse = {
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Occoreu um erro interno por favor tente mais tarde.'
                }
                return reply(infoResponse)
            })
        }
    },

    RemoveMember: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            RemoveMemberRequest(request.session.sessiontoken, request.payload.memberid)
            .then(DoneObject => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'team'
                }
                return reply(infoResponse)
            })
            .catch(error => {
                var infoResponse = {
                    statusCode: 500,
                    error: 'Internal Error',
                    message: 'Occoreu um erro interno por favor tente mais tarde.'
                }
                return reply(infoResponse)
            })
        }
    },

    AddNewMember: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect(new Error('400'));
        }
        else {
            AddNewMemberRequest(request.payload.email, request.payload.firstname, request.payload.lastname, request.session.teamID, request.payload.role, request.session.sessiontoken)
            .then(NewMember => {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'team'
                }
                return reply(infoResponse)
            })
            .catch(error => {
                if(error['message'] === 'email in use') {
                    var infoResponse = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'Já existe uma conta registrada com este email.'
                    }
                    return reply(infoResponse)
                }
                else {
                    var infoResponse = {
                        statusCode: 500,
                        error: 'Internal Error',
                        message: 'Occoreu um erro interno por favor tente mais tarde.'
                    }
                    return reply(infoResponse)
                    }
            })
        }
    }
}