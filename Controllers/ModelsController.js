module.exports = {
    RenderModelsPage: function(request, reply) {
        return reply.view('models', {}, { layout: 'IndexLayout' });
    }
}